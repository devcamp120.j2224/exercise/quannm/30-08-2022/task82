$(document).ready(function() {
    var gArrayCount;
    var gArrayCountry = [];
    var gArrayCus = [];
    onPageLoading();
    function onPageLoading() {
        callApiCountCusByCountry();
        for (var i = 0; i < gArrayCount.length; i++) {
            var gArray = gArrayCount[i];
            gArrayCountry.push(gArray[1]);
        }
        console.log(gArrayCountry);
        for (var i = 0; i < gArrayCount.length; i++) {
            var gArray = gArrayCount[i];
            gArrayCus.push(gArray[0]);
        }
        console.log(gArrayCus);
    }
    var areaChartData = {
        labels: gArrayCountry,
        datasets: [
            {
                label: 'Customer',
                backgroundColor: 'rgba(60,141,188,0.9)',
                borderColor: 'rgba(60,141,188,0.8)',
                pointRadius: false,
                pointColor: '#3b8bba',
                pointStrokeColor: 'rgba(60,141,188,1)',
                pointHighlightFill: '#fff',
                pointHighlightStroke: 'rgba(60,141,188,1)',
                data: gArrayCus
            }
        ]
    }
    // -------------
    // - BAR CHART -
    // -------------
    var barChartCanvas = $('#barChart').get(0).getContext('2d')
    var barChartData = $.extend(true, {}, areaChartData)
    var temp0 = areaChartData.datasets[0]
    barChartData.datasets[0] = temp0
    
    var barChartOptions = {
        responsive: true,
        maintainAspectRatio: false,
        datasetFill: false
    }
    
    new Chart(barChartCanvas, {
        type: 'bar',
        data: barChartData,
        options: barChartOptions
    })
    function callApiCountCusByCountry() {
        $.ajax({
            url: "http://localhost:8080/getCountCus",
            type: "GET",
            dataType: "json",
            async: false,
            success: function(res) {
                gArrayCount = res;
                console.log(gArrayCount);
            },
            error: function(error) {
                console.log(error.responseText);
            }
        });
    }
})

