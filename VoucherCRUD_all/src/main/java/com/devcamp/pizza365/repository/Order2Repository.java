package com.devcamp.pizza365.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.devcamp.pizza365.entity.Order2;

@Repository
public interface Order2Repository extends JpaRepository<Order2, Long> {

}
