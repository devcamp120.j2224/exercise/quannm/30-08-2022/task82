package com.devcamp.pizza365.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.devcamp.pizza365.entity.*;

@Repository
public interface Customer2Repository extends JpaRepository<Customer2, Integer> {
    @Query(value = "SELECT *, CONCAT_WS(' ', first_name, last_name) AS full_name FROM customers", nativeQuery = true)
    List<Customer2> getListAllCustomerByQuery();

    @Query(value = "SELECT COUNT(`id`) AS count_cus, `country` FROM `customers` GROUP BY `country`", nativeQuery = true)
    List<Object> getCountCusByCountry();
}
